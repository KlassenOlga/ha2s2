#include "Length.h"


const double Length::factors[] = { 0.001, 0.01, 0.3048, 0.9144, 1.0, 1000.0, 1609.3 };
Length::Length(double value, Unit unit)
{
	
	m_value=value* factors[unit];
}

double Length::getValue(Unit unit)
{
	return m_value/=factors[unit];
}


Length::~Length()
{
}
