#include "Prism.h"
#include <iostream>
using namespace std;


Prism::Prism(double length, double height, double width, Point3D& point) : m_length(length), m_height(height), m_width(width)
{
	this->m_point = point;
}

Prism::Prism(Length length, Length height, Length width, Point3D& point)
{
	m_length = length.getValue();
	m_height = height.getValue();
	m_width = width.getValue();
	m_point = point;
}

Prism::Prism()
{
}


Prism::~Prism()
{
}

double Prism::getVolume(Length::Unit unit) const
{

	double l = Length(0.5* m_width*m_length*m_height).getValue(unit);
	l /= Length::factors[unit];//Kubik!!!!!!
	l /= Length::factors[unit];
	return l;

}

double Prism::getSurface(Length::Unit unit) const
{
	double surface1 = m_width * m_height*0.5;
	double surface2 = m_length * m_width;
	double b = sqrt(m_height*m_height + (m_width/2)*(m_width/2));
	double surface3 = b * m_length;

	double l = Length(surface1 * 2 + surface2 + surface3 * 2).getValue(unit);
	l /= Length::factors[unit];//Quadrat!!!!!!
	return l;
}

string Prism::getBodyName() const
{
	return  "Prism";
}

Body * Prism::createByDialog()
{
	double length = 0.0;
	double height = 0.0;
	double width = 0.0;
	double x = 0.0;
	double y = 0.0;
	double z = 0.0;
	cout << "Enter the length of your prism" << endl;
	cin >> length;

	cout << "Enter the height of your prism" << endl;
	cin >> height;

	cout << "Enter the width of your prism" << endl;
	cin >> width;

	cout << "Enter the x-coordinate of the center point" << endl;
	cin >> x;
	cout << "Enter the y-coordinate of the center point" << endl;
	cin >> y;
	cout << "Enter the z-coordinate of the center point" << endl;
	cin >> z;
	Point3D point(x, y, z);

	Prism* res = new Prism(length, height, width, point);

	return res;
}


