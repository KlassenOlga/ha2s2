#include "Point3D.h"



Point3D::Point3D()
{
}

Point3D::Point3D(double x, double y, double z, Length::Unit unit)
{
	m_x = Length(x, unit).getValue();
	m_y = Length(y, unit).getValue();
	m_z = Length(z, unit).getValue();
}

Point3D::Point3D(Length x, Length y, Length z)
{
	m_x = x.getValue();
	m_y = y.getValue();
	m_z = z.getValue();
}


Point3D::~Point3D()
{
}

Point3D Point3D::getLocation() const
{
	return Point3D(m_x,m_y,m_z);
}

double Point3D::getX(Length::Unit unit) const
{

	return Length(m_x).getValue(unit);
}

double Point3D::getY(Length::Unit unit) const
{

	return Length(m_y).getValue(unit);
}

double Point3D::getZ(Length::Unit unit) const
{

	return Length(m_z).getValue(unit);
}
