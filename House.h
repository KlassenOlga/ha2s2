#pragma once
#include "Body.h"
class Prism;
class Cuboid;

class House:public Body
{
public:
	House(double heightPrism, double heightCuboid, double width, double length, Point3D& point);
	House(Length heightPrism, Length heightCuboid, Length width, Length length, Point3D& point);
	~House();
	double getVolume(Length::Unit unit = Length::METRE)const override;
	double getSurface(Length::Unit unit = Length::METRE) const override;
	string getBodyName()const override;
	static Body* createByDialog();
	const int ID=id;

private:
	Prism* m_prism;
	Cuboid* m_cuboid;



};



