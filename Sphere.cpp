#include "Sphere.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>
using namespace std;

Sphere::Sphere(double radius, Point3D& point):m_radius(radius)
{
	
	this->m_point = point;
}

Sphere::Sphere(Length radius, Point3D & point)
{
	m_radius = radius.getValue();
	m_point = point;
}


Sphere::~Sphere()
{
}

double Sphere::getVolume(Length::Unit unit) const
{
	double l = Length(4 / 3 * M_PI * m_radius*m_radius*m_radius).getValue(unit);
	l /= Length::factors[unit];//Kubik!!!!!!
	l /= Length::factors[unit];
	return l;
}

double Sphere::getSurface(Length::Unit unit) const
{
	double l = Length(4 * M_PI * m_radius * m_radius).getValue(unit);
	l /= Length::factors[unit];//Quadrat!!!!!!
	return l;
}

string Sphere::getBodyName() const
{
	return  "Sphere";
}

Body * Sphere::createByDialog()
{
	double radius = 0.0;
	double x = 0.0;
	double y = 0.0;
	double z = 0.0;
	cout << "Enter the radius of your sphere" << endl;
	cin >> radius;
	cout << "Enter the x-coordinate of the center point" << endl;
	cin >> x;
	cout << "Enter the y-coordinate of the center point" << endl;
	cin >> y;
	cout << "Enter the z-coordinate of the center point" << endl;
	cin >> z;
	Point3D point(x, y, z);

	Sphere* res = new Sphere(radius, point);
	return res;

}


