#pragma once
#include "Body.h"

class Cuboid :
	public Body
{
public:
	Cuboid();
	Cuboid(double length, double height, double width, Point3D& point);
	Cuboid(Length length, Length height, Length width, Point3D& point);
	~Cuboid();
	double getVolume(Length::Unit unit = Length::METRE)const override;
	double getSurface(Length::Unit unit = Length::METRE) const override;
	string getBodyName()const override;
	static Body* createByDialog();
	const int ID=id;
private:
	double m_length;
	double m_height;
	double m_width;
};

