#include "Cube.h"
#include <iostream>
using namespace std;


Cube::Cube(double side, Point3D& point)
{
	this->m_point = point;
	this->m_side = side;
	

}

Cube::Cube(Length side, Point3D & point)
{
	m_side = side.getValue();
	m_point = point;
}


Cube::~Cube()
{
}

double Cube::getVolume(Length::Unit unit) const
{
	double l = Length(m_side*m_side*m_side).getValue(unit);
	l /= Length::factors[unit];//Kubik!!!!!!
	l /= Length::factors[unit];
	return l;
}

double Cube::getSurface(Length::Unit unit) const
{
	double l = Length(6 * m_side*m_side).getValue(unit);
	l /= Length::factors[unit];//Quadrat!!!!!!
	return l;

}

string Cube::getBodyName() const
{
	return "Cube";
}

Body * Cube::createByDialog()
{
	double side = 0.0;

	double x = 0.0;
	double y = 0.0;
	double z = 0.0;
	cout << "Enter the length of the side of your cone" << endl;
	cin >> side;

	cout << "Enter the x-coordinate of the center point" << endl;
	cin >> x;
	cout << "Enter the y-coordinate of the center point" << endl;
	cin >> y;
	cout << "Enter the z-coordinate of the center point" << endl;
	cin >> z;
	Point3D point(x, y, z);

	Cube* res = new Cube(side, point);
	return res;
}


