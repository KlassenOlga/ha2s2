#pragma once
#include "Body.h"

class Cube :
	public Body
{
public:
	Cube(double side, Point3D& point);
	Cube(Length side, Point3D& point);
	~Cube();
	double getVolume(Length::Unit unit = Length::METRE)const override;
	double getSurface(Length::Unit unit = Length::METRE) const override;
	string getBodyName()const override;
	static Body* createByDialog();
	const int ID=id;

private:

	double m_side;
};

