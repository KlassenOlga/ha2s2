#include "Cone.h"
#define _USE_MATH_DEFINES
#include <math.h>//seltsamerweise akteptiert der Kompiler cmath nicht
#include <iostream>

using namespace std;



Cone::Cone(double radius, double height, Point3D& point): m_radius(radius),m_height(height)
{
	this->m_point = point;

}

Cone::Cone(Length radius, Length height, Point3D & point)
{
	m_radius = radius.getValue();
	m_height = height.getValue();
	m_point = point;
}


 Cone::~Cone()
 {
 }


double Cone::getVolume(Length::Unit unit) const
{

 	double l = Length(M_PI* m_radius*m_radius* m_height / 3).getValue(unit);
	l /= Length::factors[unit];//Kubik!!!!!!
	l /= Length::factors[unit];
	return l;
}

double Cone::getSurface(Length::Unit unit) const
{
	double l = Length(M_PI*m_radius * (m_radius + sqrt(m_height*m_height + m_radius * m_radius))).getValue(unit);
	l /= Length::factors[unit];//Quadrat!!!!!!
	return l;
}

string Cone::getBodyName() const
{
	return "Cone";
}

Body * Cone::createByDialog()
{
	double radius=0.0;
	double height = 0.0;
	double x = 0.0;
	double y = 0.0;
	double z = 0.0;
	cout << "Enter the radius of your cone" << endl;
	cin >> radius;

	cout << "Enter the height of your cone" << endl;
	cin >> height;

	cout << "Enter the x-coordinate of the center point" << endl;
	cin >> x;
	cout << "Enter the y-coordinate of the center point" << endl;
	cin >> y;
	cout << "Enter the z-coordinate of the center point" << endl;
	cin >> z;
	Point3D point(x,y,z);
	
	Cone* res = new Cone(radius, height, point);
	return res;
}


