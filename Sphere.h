#pragma once
#include "Body.h"
class Sphere :
	public Body
{
public:
	Sphere(double radius, Point3D& point);
	Sphere(Length radius, Point3D& point);
	~Sphere();
	double getVolume(Length::Unit unit = Length::METRE)const override;
	double getSurface(Length::Unit unit = Length::METRE) const override;
	string getBodyName()const override;
	static Body* createByDialog();
	const int ID=id;
private:
	double m_radius;
};

