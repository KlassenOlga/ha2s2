#include "Cylinder.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>
using namespace std;

Cylinder::Cylinder(double height, double radius, Point3D& point):m_radius(radius), m_height(height)
{
	this->m_point = point;

}

Cylinder::Cylinder(Length height, Length radius, Point3D & point)
{
	m_height = height.getValue();
	m_radius = radius.getValue();
	m_point = point;
}


Cylinder::~Cylinder()
{
}

double Cylinder::getVolume(Length:: Unit unit) const
{

	double l = Length(M_PI*m_radius*m_radius*m_height).getValue(unit);
	l /= Length::factors[unit];//Kubik!!!!!!
	l /= Length::factors[unit];
	return l;
	return M_PI*m_radius*m_radius*m_height;
}

double Cylinder::getSurface(Length::Unit unit) const
{
	double l = Length(2 * M_PI*m_radius* (m_radius + m_height)).getValue(unit);
	l /= Length::factors[unit];//Quadrat!!!!!!
	return l;
}

string Cylinder::getBodyName() const
{
	return "Cylinder";
}

Body * Cylinder::createByDialog()
{
	double radius = 0.0;
	double height = 0.0;
	double x = 0.0;
	double y = 0.0;
	double z = 0.0;
	cout << "Enter the radius of your cylinder" << endl;
	cin >> radius;

	cout << "Enter the height of your cylinder" << endl;
	cin >> height;

	cout << "Enter the x-coordinate of the center point" << endl;
	cin >> x;
	cout << "Enter the y-coordinate of the center point" << endl;
	cin >> y;
	cout << "Enter the z-coordinate of the center point" << endl;
	cin >> z;
	Point3D point(x, y, z);

	Cylinder* res = new Cylinder(height,radius, point);
	
	return res;
}


