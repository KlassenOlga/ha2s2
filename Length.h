#pragma once
class Length
{
public:
	enum Unit { MILLIMETERE, CENTIMETER, FOOT, YARD, METRE, KILOMETRE, MILE };
	static const double factors[];
	Length(double value=0.0, Unit unit=METRE);
	double getValue(Unit unit=METRE);
	~Length();
private:
	double m_value;
};

