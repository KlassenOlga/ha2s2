#include "Tower.h"
#include "Cylinder.h"
#include "Cuboid.h"
#include "Cone.h"
#include <iostream>
using namespace std;

Tower::Tower(double heightCylinder, double heightCone, double radius, Point3D& point)
{

	Cylinder dummyCylinder(heightCylinder, radius, point);
	Point3D pointDummyCone(point.getX(), point.getY()+heightCylinder/2+heightCone/4, point.getZ());
	Cone dummyCone(radius, heightCone, pointDummyCone);
	double y = (point.getY()*dummyCylinder.getVolume() + pointDummyCone.getY()*dummyCone.getVolume()) / dummyCone.getVolume() + dummyCylinder.getVolume();
	this->m_point = point;
	double difference = y- point.getY();
	Point3D pointCylinder(point.getX(), point.getY() - difference, point.getZ());
	Point3D pointCone(point.getX(), pointDummyCone.getY()-difference, point.getZ());

	m_cylinder = new Cylinder(heightCylinder, radius, pointCylinder);
	m_cone = new Cone(radius, heightCone, pointCone);


}

Tower::Tower(Length heightCylinder, Length heightCone, Length radius, Point3D& point) {

	Cylinder dummyCylinder(heightCylinder, radius, point);
	Point3D pointDummyCone(point.getX(), point.getY() + heightCylinder.getValue() / 2 + heightCone.getValue() / 4, point.getZ());
	Cone dummyCone(radius, heightCone, pointDummyCone);
	double y = (point.getY()*dummyCylinder.getVolume() + pointDummyCone.getY()*dummyCone.getVolume()) / dummyCone.getVolume() + dummyCylinder.getVolume();
	this->m_point = point;
	double difference = y - point.getY();
	Point3D pointCylinder(point.getX(), point.getY() - difference, point.getZ());
	Point3D pointCone(point.getX(), pointDummyCone.getY() - difference, point.getZ());

	m_cylinder = new Cylinder(heightCylinder, radius, pointCylinder);
	m_cone = new Cone(radius, heightCone, pointCone);
}
Tower::~Tower()
{
	delete m_cone;
	delete m_cylinder;
}

string Tower::getBodyName() const
{
	return "Tower";
}

double Tower::getVolume(Length::Unit unit) const
{
	double l = Length(m_cone->getVolume() + m_cylinder->getVolume()).getValue(unit);
	l /= Length::factors[unit];//Kubik!!!!!!
	l /= Length::factors[unit];
	return l;
}

double Tower::getSurface(Length::Unit unit ) const
{
	double l = Length(m_cone->getSurface() + m_cylinder->getSurface(), unit).getValue(unit);
	l /= Length::factors[unit];//Quadrat!!!!!!
	return l;
}

Body * Tower::createByDialog()
{
	double heightCylinder = 0.0;
	double heightCuboid = 0.0;
	double radius;

	cout << "Enter the height of cylinder of your tower" << endl;
	cin >> heightCylinder;

	cout << "Enter the height of cuboid of your tower" << endl;
	cin >> heightCuboid;

	cout << "Enter the radius of your tower" << endl;
	cin >> radius;
	double x = 0.0;
	double y = 0.0;
	double z = 0.0;
	cout << "Enter the x-coordinate of the center point" << endl;
	cin >> x;
	cout << "Enter the y-coordinate of the center point" << endl;
	cin >> y;
	cout << "Enter the z-coordinate of the center point" << endl;
	cin >> z;
	Point3D point(x, y, z);

	Tower* res = new Tower(heightCylinder, heightCuboid, radius, point);
	return res;
}

