#pragma once
#include "Body.h"
#include <queue>
using std::queue;
class BodyQueue
{
public:
	BodyQueue();
	~BodyQueue();
	void enqueue(const Body& body);
	const Body* dequeue();
	//queue<const Body*> getQueue();
	bool empty();
private:
	queue<const Body*> m_bodyQueue;

};

