#pragma once
#include "Body.h"

class Cylinder: public Body
{
public:
	Cylinder(double height, double radius, Point3D& point);
	Cylinder(Length height, Length radius, Point3D& point);

	~Cylinder();
	double getVolume(Length::Unit unit = Length::METRE)const override;
	double getSurface(Length::Unit unit = Length::METRE) const override;
	string getBodyName()const override;
	static Body* createByDialog();
	const int ID=id;
public:
	double m_height;
	double m_radius;
};

