#pragma once
#include "Body.h"
#include <string>
#include "Length.h"

using std::string;
class Cone :
	public Body
{
public:
	Cone(double radius, double height, Point3D& point);
	Cone(Length radius, Length height, Point3D& point);
 	~Cone();

 	double getVolume(Length::Unit unit=Length::METRE)const override;
	double getSurface(Length::Unit unit = Length::METRE) const override;

	string getBodyName()const override;
	static Body* createByDialog();
	const int ID =id;
private:
	double m_radius;
	double m_height;
};

