#pragma once
#include "Point3D.h"
#include <string>
using std::string;

class Body
{
public:
	Body();
	~Body();
	virtual double getVolume(Length::Unit unit = Length::METRE) const = 0;
	virtual double getSurface(Length::Unit unit = Length::METRE) const = 0;
	virtual string getBodyName()const = 0;
	Point3D getLocation() const;
protected:
	static  int id;
	Point3D m_point;

};

