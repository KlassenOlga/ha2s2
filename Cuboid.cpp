#include "Cuboid.h"
#include <iostream>
using namespace std;


Cuboid::Cuboid()
{
}

Cuboid::Cuboid(double length, double height, double width, Point3D& point): m_length(length), m_height(height), m_width(width)
{
	this->m_point = point;

}

Cuboid::Cuboid(Length length, Length height,Length width, Point3D& point)
{
	m_length = length.getValue();
	m_height = height.getValue();
	m_width = width.getValue();
	m_point = point;
}


Cuboid::~Cuboid()
{
}

double Cuboid::getVolume(Length::Unit unit) const
{
	double l = Length(m_length*m_height*m_width).getValue(unit);
	l /= Length::factors[unit];//Kubik!!!!!!
	l /= Length::factors[unit];
	return l;
	
}

double Cuboid::getSurface(Length::Unit unit) const
{
	double l = Length(2 * m_width*m_length + 2 * m_length*m_height + 2 * m_height*m_width).getValue(unit);
	l /= Length::factors[unit];//Quadrat!!!!!!
	return l;

}

string Cuboid::getBodyName() const
{
	return "Cuboid";
}

Body * Cuboid::createByDialog()
{
	double length = 0.0;
	double height = 0.0;
	double width = 0.0;
	double x = 0.0;
	double y = 0.0;
	double z = 0.0;
	cout << "Enter the length of your cuboid" << endl;
	cin >> length;

	cout << "Enter the height of your cuboid" << endl;
	cin >> height;

	cout << "Enter the width of your cuboid" << endl;
	cin >> width;

	cout << "Enter the x-coordinate of the center point" << endl;
	cin >> x;
	cout << "Enter the y-coordinate of the center point" << endl;
	cin >> y;
	cout << "Enter the z-coordinate of the center point" << endl;
	cin >> z;
	Point3D point(x, y, z);

	Cuboid* res = new Cuboid(length, height, width, point);
	return res;
}


