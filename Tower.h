#pragma once
#include "Body.h"
class Cone;
class Cylinder;
 

class Tower:public Body
{
public:
	Tower(double heightCylinder, double heightCone, double radius, Point3D& point);
	Tower(Length heightCylinder, Length heightCone, Length radius, Point3D& point);
	~Tower();
	string getBodyName()const override;
	double getVolume(Length::Unit unit = Length::METRE)const override;
	double getSurface(Length::Unit unit = Length::METRE) const override;
	static Body* createByDialog();
	const int ID=id;
private:
	Cylinder* m_cylinder;
	Cone* m_cone;
};

