#include "Body.h"
#include "House.h"
#include "Cuboid.h"
#include "Prism.h"
#include <iostream>
using namespace std;

House::House(double heightPrism, double heightCuboid, double width, double length, Point3D& point)//schwerP von Cuboid
{
 	Cuboid cuboidDummy(length, heightCuboid, width, point);//wir erstellen ersmal ein Cuboid mit dem swerpunkt des Houses
 	Point3D prismaDummyPoint(point.getX(), point.getY()+heightCuboid/2+heightPrism/3, point.getZ());
 	Prism prismDummy(length, heightPrism, width, prismaDummyPoint);//Davon ausgegangen k�nnen wir prisma erstellen
 	double y = (point.getY() * cuboidDummy.getVolume() + prismaDummyPoint.getY() * prismDummy.getVolume()) / (prismDummy.getVolume() + cuboidDummy.getVolume());
 	double difference = y - point.getY();
 	
 	Point3D pointCuboid(point.getX(), point.getY() - difference, point.getZ());
 	Point3D pointPrism(point.getX(), (point.getY() + heightCuboid / 2 + heightPrism / 3) - difference, point.getZ());
	
 	m_cuboid=new Cuboid(length, heightCuboid, width, pointCuboid);
 	m_prism=new Prism(length, heightPrism, width, pointPrism);

 	this->m_point = point;
}

House::House(Length heightPrism, Length heightCuboid, Length width, Length length, Point3D & point)
{
	Cuboid cuboidDummy(length, heightCuboid, width, point);//wir erstellen ersmal ein Cuboid mit dem swerpunkt des Houses
	Point3D prismaDummyPoint(point.getX(), point.getY() + heightCuboid.getValue() / 2 + heightPrism.getValue() / 3, point.getZ());
	Prism prismDummy(length, heightPrism, width, prismaDummyPoint);//Davon ausgegangen k�nnen wir prisma erstellen
	double y = (point.getY() * cuboidDummy.getVolume() + prismaDummyPoint.getY() * prismDummy.getVolume()) / (prismDummy.getVolume() + cuboidDummy.getVolume());
	double difference = y - point.getY();

	Point3D pointCuboid(point.getX(), point.getY() - difference, point.getZ());
	Point3D pointPrism(point.getX(), (point.getY() + heightCuboid.getValue() / 2 + heightPrism.getValue() / 3) - difference, point.getZ());

	m_cuboid = new Cuboid(length, heightCuboid, width, pointCuboid);
	m_prism = new Prism(length, heightPrism, width, pointPrism);

	this->m_point = point;
}


House::~House()
{
	delete m_prism;
	delete m_cuboid;
}

double House::getVolume(Length::Unit unit) const
{
	double l = Length(m_cuboid->getVolume() + m_prism->getVolume()).getValue(unit);
	l /= Length::factors[unit];//Kubik!!!!!!
	l /= Length::factors[unit];
	return l;
}

double House::getSurface(Length::Unit unit) const
{
	double l = Length(this->m_cuboid->getSurface() + this->m_prism->getSurface()).getValue(unit);
	l /= Length::factors[unit];//Quadrat!!!!!!
	return l;
}

string House::getBodyName() const
{
	return  "House";
}

Body * House::createByDialog()
{
	double heightPrism = 0.0;
	double heightCuboid = 0.0;
	double length = 0.0;
	double width = 0.0;

	double x = 0.0;
	double y = 0.0;
	double z = 0.0;
	cout << "Enter the height of prism of your house" << endl;
	cin >> heightPrism;

	cout << "Enter the height of cuboid of your house" << endl;
	cin >> heightCuboid;

	cout << "Enter the length of your house" << endl;
	cin >> length;

	cout << "Enter the width of your house" << endl;
	cin >> width;


	cout << "Enter the x-coordinate of the center point" << endl;
	cin >> x;
	cout << "Enter the y-coordinate of the center point" << endl;
	cin >> y;
	cout << "Enter the z-coordinate of the center point" << endl;
	cin >> z;
	Point3D point(x, y, z);

	House* res = new House(heightPrism, heightCuboid, width, length, point);
	return res;
}


