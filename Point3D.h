#pragma once
#include "Length.h"
class Point3D
{
public:
	Point3D();
	Point3D(double x, double y, double z, Length::Unit unit);
	Point3D(Length x, Length y, Length z);
	~Point3D();
	Point3D getLocation() const;
	double getX(Length::Unit unit=Length::METRE) const;
	double getY(Length::Unit unit = Length::METRE) const;
	double getZ(Length::Unit unit = Length::METRE) const;
private:
	double m_x;
	double m_y;
	double m_z;
};

