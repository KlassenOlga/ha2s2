#include "BodyQueue.h"
#include "Prism.h"


BodyQueue::BodyQueue()
{
}


BodyQueue::~BodyQueue()
{
}

void BodyQueue::enqueue(const Body& body)
{
	m_bodyQueue.push(&body);
}

const Body * BodyQueue::dequeue()
{
	const Body* b = (m_bodyQueue.front());
	m_bodyQueue.pop();

	return b;
}

// queue<const Body*> BodyQueue::getQueue()
// {
// 	return m_bodyQueue;
// }

bool BodyQueue::empty()
{
	return m_bodyQueue.empty();
}
